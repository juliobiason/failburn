#!/usr/bin/python
# -*- coding: utf-8 -*-
#

import csv
import urllib
import urllib2

#url = 'http://www1.previdencia.gov.br/pg_secundarias/' \
#    'paginas_perfis/perfil_comPrevidencia_09_04-A.asp'

URL = 'http://www.previdencia.gov.br/devedores/consdeved.asp'

# fake user-agent
HEADERS = {'User-Agent': 'Mozilla/5.0 (X11; U; Linux i686; en-US; ' \
                'rv:1.9.0.2) Gecko/2008092313 Ubuntu/8.04 (hardy) ' \
                'Firefox/3.1.6'}

content = csv.reader(open('cpfsenadores.csv'), delimiter=';')
for row in content:
    if len(row) < 2:
        continue

    name = row[0]
    cpf = row[1]

    print name, cpf

    try:
        int(cpf)
    except ValueError:
        continue

    body = {
        'checkcpf': 'on',
        'cpf': cpf}

    request = urllib2.Request(url=URL)

    for key in HEADERS:
        request.add_header(key, HEADERS[key])

    body = urllib.urlencode(body)
    request.add_data(body)

    response = urllib2.urlopen(request)
    data = response.read()
    response.close()

    file('result.txt', 'w').write(data)
    break