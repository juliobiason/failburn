#!/usr/bin/python
# -*- coding: utf-8 -*-
#

import csv
import urllib
import urllib2
import time
import os
import re
import cookielib

from BeautifulSoup import BeautifulSoup

# fake user-agent
headers = {'User-Agent': 'Mozilla/5.0 (X11; U; Linux i686; en-US; ' \
                'rv:1.9.0.2) Gecko/2008092313 Ubuntu/8.04 (hardy) ' \
                'Firefox/3.1.6'}

sources = ['Inidoneas', 'Suspensas']
#sources = ['Testing']

cookie_jar = cookielib.CookieJar()
opener = urllib2.build_opener(urllib2.HTTPCookieProcessor(cookie_jar))

output = file('results.txt', 'wb')

for source in sources:
    print source + ':'
    output.write(source + '\n')

    content = csv.reader(open(source + '.csv'), delimiter=';')

    # The data have two headers
    content.next()
    content.next()
    for row in content:
        cnpj = row[0]
        name = row[1]
        print cnpj, name
        output.write('%s %s\n' % (cnpj, name))

        # remove ".", "/" and "-". Do not translate any other characters.
        #cnpj_clean = row[0].translate(None, './-')
        cnpj_clean = re.sub(r'[\./-]', '', row[0])
        result_filename = cnpj_clean + '.result'

        form = {
            'acao': 'resumo',
            'cdCpfCnpjDoador': cnpj_clean,
            'cdEspRecurso': '-1',    # any
            'dsCargo': '',
            'municipio': '',
            'nmCandidato': '',
            'nomeFornecDoador': 'Doador',
            'nrCand': '',
            'rdTipo': 'receita',
            'sgPartido': '',
            'sgUe': '',
        }

        request = urllib2.Request(url='http://www4.tse.gov.br/' \
            'spce2008ConsultaFinanciamento/' \
            'consultaReceitaDespesaCandidatoServlet.do')

        for key in headers:
            request.add_header(key, headers[key])

        body = urllib.urlencode(form)
        request.add_data(body)

        response = urllib2.urlopen(request)
        data = response.read()
        cookie = response.headers['Set-Cookie']
        response.close()

        # this is probably not very effective
        if '<iframe' in data:
            # Yup, donators.
            request = urllib2.Request(url='http://www4.tse.gov.br/' \
                '/spce2008ConsultaFinanciamento/listaReceitaCand.jsp')

            for key in headers:
                request.add_header(key, headers[key])

            request.add_header('Cookie', cookie)

            response = urllib2.urlopen(request)
            data = response.read()
            response.close()

            soup = BeautifulSoup(data)
            rows = soup.findAll('tr')

            #  0: Empty
            #  1: Donator name
            #  2: Empty
            #  3: Donator CNPJ
            #  4: Empty
            #  5: Date
            #  6: Empty
            #  7: Donation Value
            #  8: Empty
            #  9: Resource type
            # 10: Empty
            # 11: Transfer type
            # 12: Empty
            # 13: Name (and, inside it, we have a lot of <br/>s)
            # 14: Empty
            # 15: Candidate number
            # 16: Empty
            # 17: Party
            # 18: Empty
            # 19: Position
            # 20: Empty
            # 21: City
            # 22: Empty

            for data in rows[2:]:
                donation = '%s = %s (%s %s) %s' % (
                        data.contents[7].contents[0].strip(),
                        data.contents[13].contents[2].strip(),
                        data.contents[15].contents[0].strip(),
                        data.contents[17].contents[0].strip(),
                        data.contents[21].contents[0].strip())
                donation = donation.encode('utf8')
                print donation
                output.write(donation + '\n')
        print
        output.write('\n')
        output.flush()

    print
    output.write('\n')