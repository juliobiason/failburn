from BeautifulSoup import BeautifulSoup

soup = BeautifulSoup(open('07369685000197.result'))

rows = soup.findAll('tr')
#  0: Empty
#  1: Donator name
#  2: Empty
#  3: Donator CNPJ
#  4: Empty
#  5: Date
#  6: Empty
#  7: Donation Value
#  8: Empty
#  9: Resource type
# 10: Empty
# 11: Transfer type
# 12: Empty
# 13: Name
# 14: Empty
# 15: Candidate number
# 16: Empty
# 17: Party
# 18: Empty
# 19: Position
# 20: Empty
# 21: City
# 22: Empty
# 23:
for data in rows[2:]:
    if len(data.contents) < 13:
        continue

    print unicode(data.contents[13].contents[2]).strip()